from flask import Flask, flash, redirect, render_template, request, session, url_for
from gpiozero import LED

led = LED(18)

app = Flask(__name__)
    
@app.route("/")
def index():
    return render_template("index.html")
              
@app.route("/switch", methods=['POST'])
def toggle():
    led.toggle()
    return redirect(url_for("index"))

if __name__ == "__main__":
    led.off();
    app.run(host='0.0.0.0', port=80)
